# Generate [iRe] notebooks

- [region-nb.sh](region-nb.sh): runs [region.ipynb](region.ipynb), generates [NYC.ipynb](iRe/NYC.ipynb), [NJ.ipynb](iRe/NJ.ipynb)
- [rides-per-day.sh](rides-per-day.sh): runs [rides-per-day.ipynb](rides-per-day.ipynb), generates [202403.ipynb](iRe/202403.ipynb), [202404.ipynb](iRe/202404.ipynb)

```bash
export IRE_SHOW=png    # Embed PNGs of plots in Notebooks (not HTML/JS/CSS)
export IRE_RELPATH=..  # iRe artifacts are saved in .ire in this directory, but notebooks are under iRe/, so they need to go up a level to find their artifacts.
juq papermill run -p region=nyc region.ipynb -o iRe/NYC.ipynb
juq papermill run -p region=jc,hob region.ipynb -o iRe/NJ.ipynb
juq papermill run -p ym=202408 -p "drop=rideable_type_duplicate_column_name_1" -p table=rides rides-per-day.ipynb -o iRe/202408.ipynb
juq papermill run -p ym=202409 rides-per-day.ipynb -o iRe/202409.ipynb
```

Uses [juq]:
```bash
pip install juq.py>=0.2.1
```

[iRe]: https://gitlab.com/groups/runsascoded/ire
[juq]: https://github.com/runsascoded/juq
